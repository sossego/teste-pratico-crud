//Insere um registro no banco
exports.create = async(data) => {
  return await pool.query(`INSERT INTO contato (nome) VALUES (${data.nome})`);
}

//Le um registro do banco
exports.read = async(data) => {
  return await pool.query(`SELECT * FROM contato WHERE id = ${data.id}`);
}

//Atualzia um registro no banco
exports.update = async(data) => {
  return await pool.query(`UPDATE contato SET nome = ${data.nome} WHERE id = ${data.id}`);
}

//Deleta um registro do banco
exports.delete = async(data) => {
  return await pool.query(`DELETE FROM contato WHERE id = ${data.id}`);
}